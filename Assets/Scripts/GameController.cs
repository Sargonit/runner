﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

public class GameController // синглтон координирующий процес игры
{
    static GameController instance;
    bool isPlayerAlive;
    float speedUp = 0.001f; // то на сколько увеличивается скорость игры
    float baseSpeed = 0.02f; // скорость движеиня дороги (начальная) 
    int scoreToUpSpeed = 20; // Кол-во монет для увеличения скорости
    UIState UI_State;
    UIController UI_Controller;
    RoadController roadController;
    PlayerController playerController;

    GameController()
    {
        UI_Controller = GameObject.Find("Canvas").GetComponent<UIController>();
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();
        roadController = GameObject.Find("Road").GetComponent<RoadController>();
        roadController.speedRoadMove = baseSpeed;

// Подписываемся на значения играющие ключевые роли (являющиеся тригерами)
        UI_Controller.CurrentUIState
           .ObserveEveryValueChanged(observable => observable.Value) // отслеживаем изменения CurrentUIState (состояние меню)
           .Subscribe(value =>  
           {
               UI_State = value;
               reactionOnChangeUIState();
           });
        UI_Controller.restart = restartGame; // при клике на кнопку Restart в игровом меню, начинаем игру заново

        playerController.ProgressInfo.CountCoinCollect // отслеживаем изменение количества cобранных монет
            .ObserveEveryValueChanged(observable => observable.Value)
            .Subscribe(value =>
            {
                upSpeedRoadByScore(value);
                UI_Controller.ScoreText.text = value.ToString();
            });
        
        playerController.ProgressInfo.IsPlayerAlive // отслеживаем "смерть" игрока
           .ObserveEveryValueChanged(observable => observable.Value)
           .Subscribe(value =>
           {
               isPlayerAlive = value;
               reactionOnChangePlayerDeath();
           });
    }

    public static GameController Instance 
    {
        get 
        {
            if (instance == null)
                instance = new GameController();
            return instance;
        }
    }
    
    void reactionOnChangeUIState() // реакция игры на изменение состояний меню
    {
        switch (UI_State)
        {
            case UIState.MainMenu: // когда мы переходим в главное меню - игрок "отключается", дорога обнуляется
                playerController.freezePlayer();
                roadController.resetRoad();
                break;
            case UIState.GameMenu: // в игровом меню, движение дороги останавливается (todo: отключить возможность управлять персонажем)
                roadController.stopMovinRoad();                 
                break;
            case UIState.GameUI: // разморозить движение дороги или создать новую, активировать персонажа, начать движение дороги      
                roadController.initializeRoad();                
                playerController.activatePlayer();              
                roadController.startMovinRoad();                
                break;
            default:
                break;
        }
    }

    void reactionOnChangePlayerDeath() 
    {
        if (!isPlayerAlive) // сбрасываем персонажа и обнуляем дорогу
        {
            UI_Controller.changeState(UIState.GameMenu);        
            roadController.resetRoad();                         
        }
    }

    void restartGame() // обнуляем состояние персонажа, обнуляем дорогу
    {
        playerController.freezePlayer();
        roadController.resetRoad();
    }

    void upSpeedRoadByScore(int score) // зависимость скорости дороги от количества собраных монет
    {
        float additional = score % scoreToUpSpeed;
        if (score != 0 && additional == 0)
        {
            roadController.speedRoadMove = baseSpeed + speedUp * (score / scoreToUpSpeed);
        }
    }
}
