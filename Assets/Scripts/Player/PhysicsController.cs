﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsController : MonoBehaviour // управление физикой персонажа и его положением
{
    float forceSidewayMove = 200f; // сила смещения при движении по земле
    float forceJump = 180f; // сила смещения в верх при прижке
    float forceJumpSide = 100f; // сила смещения в сторону при прижке
    bool isOnRoad = false; // находимся на дороге или нет
    bool isMoveLeft = false; // начали движение в лево
    bool isMoveRight = false; // начали движение в право

    // координаты по оси Х
    float leftSide = -1.5f; // для левой полосы
    float rightSide = 1.5f; // для правой полосы
    float centerSide = 0; // для средней/центр

    Rigidbody rigidbody;

    void Awake()
    {
        rigidbody = gameObject.GetComponent<Rigidbody>();
    }

    void OnCollisionEnter(Collision collision)
    {
        // выравниваем плеера по одной из границ после прыжка
        stopLeftSide();
        stopRightSide();
        stopCenterSide();
    }

    void OnCollisionExit(Collision collision) // проверка ухода с текущего блока дороги
    {
        isOnRoad = !(collision.gameObject.name == "RoadPlate");
    }

    private void OnCollisionStay(Collision collision) 
    {
        isOnRoad = (collision.gameObject.name == "RoadPlate"); // находимся ли мы на дороге (а не в воздухе)

        // останавливаем воздействие мил на плеера и выравниваем его по одной из границ при движении в лево/право
        if (isMoveLeft)
        {
            stopLeftSide();            
        }
        if (isMoveRight)
        {
            stopRightSide();
            
        }

        if (isMoveLeft || isMoveRight)
        {
            stopCenterSide();            
        }

    }

    public void jumpUp() // прыжок на месте
    {
        if (isOnRoad)
            rigidbody.AddForce(Vector3.up * forceJump);
    }

    public void jumpLeft() // прыжок в лево
    {
        if (isOnRoad)
        {
            rigidbody.AddForce(Vector3.up * forceJump);
            rigidbody.AddForce(-(Vector3.right * forceJumpSide));
        }        
    }

    public void jumpRight() // прыжок в право
    {
        if (isOnRoad)
        {
            rigidbody.AddForce(Vector3.up * forceJump);
            rigidbody.AddForce(Vector3.right * forceJumpSide);
        }
    }

    public void moveLeft() // двигаемся в лево пока нажата кнопка
    {
        if (isOnRoad)
        {
            isMoveLeft = true;
            rigidbody.AddForce(-(Vector3.right * forceSidewayMove));
        }
    }

    public void moveRight() // двигаемся в право пока нажата кнопка
    {
        if (isOnRoad)
        {
            isMoveRight = true;
            rigidbody.AddForce(Vector3.right * forceSidewayMove);
        } 
    }

    void stopLeftSide() // отцентровываем по левой границе
    {
        if (gameObject.transform.position.x <= -1.45f && gameObject.transform.position.x >= -1.55f)
        {
            gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            gameObject.transform.localPosition = new Vector3(leftSide, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
            isMoveLeft = false;
        }
    }

    void stopRightSide() // отцентровываем по правой границе
    {
        if (gameObject.transform.position.x <= 1.55f && gameObject.transform.position.x >= 1.45f)
        {
            gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            gameObject.transform.localPosition = new Vector3(rightSide, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
            isMoveRight = false;
        }
    }

    void stopCenterSide() // отцентровываем по центру
    {
        if (gameObject.transform.position.x <= 0.05f && gameObject.transform.position.x >= -0.05f)
        {
            gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            gameObject.transform.localPosition = new Vector3(centerSide, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
            isMoveLeft = false;
            isMoveRight = false;
        }
    }
}