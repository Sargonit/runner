﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class PlayerController : MonoBehaviour // назначение клавишь управление и связыванием их с действиями
{
    GameObject playerModel; // модель персонажа
    Vector3 startPosition = new Vector3(0, 0, 0); 
    Rigidbody playerModelRigidbody; 
    PhysicsController playerModelPhysicsController;
    ProgressInfo progressInfo;

    public ProgressInfo ProgressInfo { get => progressInfo; }    

    void Awake()
    {
        playerModel = gameObject.transform.GetChild(0).gameObject; // получаем модель персонажа
        playerModelRigidbody = playerModel.GetComponent<Rigidbody>();
        playerModelPhysicsController = playerModel.GetComponent<PhysicsController>();
        progressInfo = playerModel.GetComponent<ProgressInfo>();   
        
        progressInfo.IsPlayerAlive
           .ObserveEveryValueChanged(observable => observable.Value)
           .Subscribe(value =>
           {
               if (!value)
               {
                   freezePlayer();
               }
           });
    }

    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.Keypad4))  // в лево
        {
            playerModelPhysicsController.moveLeft();
        }       
        if (Input.GetKeyDown(KeyCode.Keypad6)) // в право
        {
            playerModelPhysicsController.moveRight();
        }
        if (Input.GetKeyDown(KeyCode.Keypad8)) // прыжок на месте
        {
            playerModelPhysicsController.jumpUp();
        }            
        if(Input.GetKeyDown(KeyCode.Keypad7)) // прыхок в лево
        {
            playerModelPhysicsController.jumpLeft();
        }
        if (Input.GetKeyDown(KeyCode.Keypad9)) // прыхок в право
        {
            playerModelPhysicsController.jumpRight();
        }
    }

    public void activatePlayer() // активируем персонажа и сбравсываем настройки
    {
        playerModelRigidbody.isKinematic = false;
        progressInfo.resetProgress();
    }

    public void freezePlayer() // деактивируем персонажа
    {
        playerModelRigidbody.isKinematic = true;
        playerModel.transform.localPosition = startPosition;
    }
}
