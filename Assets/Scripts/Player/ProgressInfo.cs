﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
public class ProgressInfo : MonoBehaviour // контроль сбора монет и смерти
{
    public ReactiveProperty<bool> IsPlayerAlive { get; private set; } // жив/мертв персонаж
    public ReactiveProperty<int> CountCoinCollect { get; private set; } // количество собранных монет

    void Awake()
    {
        IsPlayerAlive = new ReactiveProperty<bool>(true);
        CountCoinCollect = new ReactiveProperty<int>(0);
    }

    void Update()
    {
        if (gameObject.transform.position.y < -1.6f) // проверка на падение с дороги (todo: переделать под onTriggerExit->Road)
        {
            IsPlayerAlive.Value = false;
            CountCoinCollect.Value = 0;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.name) // столкновение с элементами дороги
        {
            case "Coin": // Игрок сталкивается с монетой
                CountCoinCollect.Value++;
                break;
            case "Block": // Игрок сталкивается с препятствием "GameOver"
                IsPlayerAlive.Value = false;
                CountCoinCollect.Value = 0;
                break;
            default:
                break;
        }        
    }

    public void resetProgress() // сброс прогресса
    {
        IsPlayerAlive.Value = true;
        CountCoinCollect.Value = 0;
    }
}
