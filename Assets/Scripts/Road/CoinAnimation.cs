﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAnimation : MonoBehaviour // вращение монеты вокруг своей оси
{
/*
 * TODO: реализовать сброс угла поворота при добавлении монеты опять на сцену (что бы все монеты вращались одинаково)
 */
    Quaternion rotationY = Quaternion.AngleAxis(1, Vector3.up);
    void Update()
    {
        transform.rotation *= rotationY;
    }
}
