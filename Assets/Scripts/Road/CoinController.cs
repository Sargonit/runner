﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour // Возврат монеты в пул объектов
{
    public delegate void PutToPool (GameObject coin);
    public PutToPool putToPool;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "PlayerModel") // возврат монеты в пул монет когда персонаж столкнулся с ней
        {
            putToPool(gameObject);
        }
    }
}
