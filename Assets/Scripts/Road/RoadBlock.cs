﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MapLegend
{
    FreePlace = 0,
    Obstruction = 1,
    Coin = 2
}
public class RoadBlock : MonoBehaviour // насройка RoadBlock (карта координат для элементов и самих элементов)
{
    /*
     * TODO: вырезать из префаба и кода connectionBottom 
     * TODO: неизменные переменные/параметры - через легковес или вынести в отдельный класс и брать по ссылке (CoordsElementOnMap/row/column/...)
     */
    Transform road;
    Transform connectionTop;

    float lengthRB; // Длина блока дороги
    float widthRB; // ширина блока дороги
    
    int row = 12; // количество элементов дороги по высоте
    int column = 3; // кол-во элементов дороги по ширине

    MapLegend[,] elementMap; // карта элементов дороги для текущего блока дороги
    Vector3[,] coordsElementOnMap; // координаты расположения элементов

    public Vector3[,] CoordsElementOnMap { get => coordsElementOnMap; set => coordsElementOnMap = value; }
    public int Row { get => row; }
    public int Column { get => column; }
    public Transform ConnectionTop { get => connectionTop; set => connectionTop = value; }
    internal MapLegend[,] ElementMap { get => elementMap; set => elementMap = value; }

    void Awake() 
    {
    // Для каждого блока дороги настраиваем карту координат элементов и карту элементов    
        road = gameObject.transform.Find("RoadPlate");
        ConnectionTop = gameObject.transform.Find("ConnectionTop");

        elementMap = new MapLegend[Row, Column];
        CoordsElementOnMap = new Vector3[Row, Column];

        lengthRB = road.transform.localScale.z;
        widthRB = road.transform.localScale.x;

        // расстояние между элементами на дороге
        float distLength = lengthRB / (Row + 1);  // по длине
        float distWidth = widthRB / (Column + 1); // по ширине

        // минимальные координаты по Z/X
        float minZ = -(lengthRB / 2); 
        float minX = -(widthRB / 2);

        // Заполнение карты координат и элементов
        float currPosZ = minZ + distLength;
        for (int i = 0; i < Row; i++, currPosZ += distLength)
        {
            float currPosX = minX + distWidth;
            for (int j = 0; j < Column; j++, currPosX += distWidth)
            {
                CoordsElementOnMap[i, j] = new Vector3(currPosX, 0, currPosZ);
                elementMap[i, j] = MapLegend.FreePlace;
            }
        }
    }

    private void OnDisable() // когда помещаем блок дороги в пулл обнуляем карту элементов
    {
        for (int i = 0; i < Row; i++)
            for (int j = 0; j < Column; j++)
                elementMap[i, j] = MapLegend.FreePlace;
    }    
}
