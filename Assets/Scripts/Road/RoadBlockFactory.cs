﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeRoadBlock // типы блоков дороги
{
    Deafult,
    Start
}
public class RoadBlockFactory // Настраивает блока дороги и его елементов (создание, размещение на сцене)
{
/*
 * TODO: Оптимизировать
 */
    public delegate void AddRoadBlockToRoad(GameObject gameObject);
    public AddRoadBlockToRoad addRoadBlockToRoad;

    Vector3 startPosition = new Vector3(0, 0, 0);
    GameObject currentRoadBlock;                                        // блок дороги с которым работаем
    RoadBlockPoolController roadBlockPoolController;                    // контроль возврата RoadBlock в пул
    RoadBlock roadBlock;                                                // скрипт настроек RoadBlock
    RoadElementBuilder roadElementBuilder = new RoadElementBuilder();   // строитель елементов RoadBlock
    Queue<GameObject> roadBlockPool = new Queue<GameObject>();          // храним не используемые RoadBlock
    Queue<GameObject> coinPool = new Queue<GameObject>();               // храним не используемые Coin 
    Queue<GameObject> obstructionPool = new Queue<GameObject>();        // храним не используемые Obstruction

    public RoadBlockFactory(){}

    public GameObject getRoadBlock(TypeRoadBlock typeRoadBlock) // получение настроенного и готового к использованию блока дороги
    {
        // Создаем или берем из пула, свободный блок дороги
        if (roadBlockPool.Count == 0)
        {
            currentRoadBlock = roadElementBuilder.createElement("RoadBlock");
            roadBlockPoolController = currentRoadBlock.GetComponent<RoadBlockPoolController>();
            roadBlockPoolController.putRoadBlockToPool = putRoadBlockToPool;           
        }
        else
        {
            currentRoadBlock = roadBlockPool.Dequeue();
            currentRoadBlock.SetActive(true);
        }
        currentRoadBlock.transform.position = startPosition;
        roadBlock = currentRoadBlock.GetComponent<RoadBlock>();
        addElementOnRoadBlock(typeRoadBlock); // Добавляем монеты и препятствия

        return currentRoadBlock;
    }

    public void makeOutRoadBlock(GameObject roadBlock) // разобрать RoadBlock на елементы и поместить в соответствующие пулы
    {
        roadBlock.transform.position = startPosition;
        roadBlock.SetActive(false);

        GameObject roadElement;
        for (int i = roadBlock.transform.childCount - 1; i > 2; i--)
        {
            roadElement = roadBlock.transform.GetChild(i).gameObject;
            putElementToPool(roadElement);
        }

        roadBlockPool.Enqueue(roadBlock);
    }

    // TODO: упростить метод, сделать меньше, разбить на части
    void addElementOnRoadBlock(TypeRoadBlock typeRoadBlock) // добавление монет/препятствий на дорогу
    {
        if (typeRoadBlock == TypeRoadBlock.Start) return; // если тип блока Start - оставляем его пустым

        int maxElementOnRoadBlock = roadBlock.Row * roadBlock.Column; // считаем максимальное кол-во элементов на дороге
        int maxObstructions = Mathf.RoundToInt(maxElementOnRoadBlock / 3); // макс-е кол-во препятствий
        int maxCoins = maxElementOnRoadBlock - maxObstructions; // макс-е кол-во монет

        // генерируем рандомно кол-во препятствий/монет
        int countOfObstructions = Random.Range(0, maxObstructions); 
        int countOfCoins = Random.Range(0, maxCoins);

        // заполняем карту препятствиями
        while (countOfObstructions > 0)
        {
            int row = Random.Range(0, roadBlock.Row);
            int column = Random.Range(0, roadBlock.Column);

            if (roadBlock.ElementMap[row, column] == MapLegend.FreePlace)
            {
                roadBlock.ElementMap[row, column] = MapLegend.Obstruction;
                countOfObstructions--;
            }
        }

        // заполняем карту монетами
        while (countOfCoins > 0)
        {
            int row = Random.Range(0, roadBlock.Row);
            int column = Random.Range(0, roadBlock.Column);

            if (roadBlock.ElementMap[row, column] == MapLegend.FreePlace)
            {
                roadBlock.ElementMap[row, column] = MapLegend.Coin;
                countOfCoins--;
            }
        }

        // заполняем дорогу препятствиями/монетами
        Vector3 coords;
        GameObject element;
        for (int i = 0; i < roadBlock.Row; i++)
        {
            for (int j = 0; j < roadBlock.Column; j++)
            {
                switch (roadBlock.ElementMap[i, j])
                {
                    case MapLegend.Obstruction:
                        coords = roadBlock.CoordsElementOnMap[i, j];
                        element = getObstruction();
                        element.transform.parent = currentRoadBlock.transform;
                        element.transform.localPosition = new Vector3(coords.x, 0.25f, coords.z);
                        element.SetActive(true);
                        break;
                    case MapLegend.Coin:
                        coords = roadBlock.CoordsElementOnMap[i, j];
                        element = getCoin();
                        element.transform.parent = currentRoadBlock.transform;
                        element.transform.localPosition = new Vector3(coords.x, 0.4f, coords.z);
                        element.SetActive(true);
                        break;
                    default:
                        break;
                }
            }
        }
    }    

    void putRoadBlockToPool(GameObject roadBlock) // закидываем дорогу в пул и создаем новую в конце дороги. TODO: убрать добавление нового блока в дорогу
    {
        makeOutRoadBlock(roadBlock);

        GameObject newRoadBlock = getRoadBlock(TypeRoadBlock.Deafult);
        addRoadBlockToRoad(newRoadBlock);
    }

    void putElementToPool(GameObject roadElement) // возвращаем жлемент дороги в соотв-й пул
    {
        roadElement.SetActive(false);
        roadElement.transform.parent = null;

        switch (roadElement.name)
        {
            case "Coin":
                coinPool.Enqueue(roadElement);
                break;
            case "Obstruction":
                obstructionPool.Enqueue(roadElement);
                break;
            default:
                break;
        }                
    }

    GameObject getCoin() // получить монету 
    {
        // берем из пула или создаем и настраиваем
        GameObject coin;
        if (coinPool.Count == 0) 
        {
            coin = roadElementBuilder.createElement("Coin");
            coin.GetComponent<CoinController>().putToPool = putElementToPool;
        }
        else
        {
             coin = coinPool.Dequeue();
        }
        return coin;        
    }

    GameObject getObstruction() // получить препятствие
    {        
        return (obstructionPool.Count == 0) ? roadElementBuilder.createElement("Obstruction") : obstructionPool.Dequeue();
    }
}
