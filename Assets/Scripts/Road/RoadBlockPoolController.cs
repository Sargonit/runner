﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadBlockPoolController : MonoBehaviour // Возврат блока дороги в пул когда плеер покинул участок дороги
{
    public delegate void PutRoadBlockToPool(GameObject gameObject);
    public PutRoadBlockToPool putRoadBlockToPool;    

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            putRoadBlockToPool(gameObject);
        }
    }
}
