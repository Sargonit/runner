﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadController : MonoBehaviour // управление все йдорогой
{
    const int countRoadBlock = 3; // базовое количество блоков дороги
    public float speedRoadMove; // скорость движеиня дороги (начальная) 
    bool isRoadMoving = false; // триггер, состояния дороги (стоит/движеться)

    RoadFactory roadFactory;

    void Awake()
    {
        roadFactory = new RoadFactory(gameObject, countRoadBlock); // задаем фабрике, сколько блоков дороги должно быть в дороге и куда их вернуть
    }

    public void initializeRoad() // построить дорогу с нуля
    {
        roadFactory.initializeRoad();
    }

    public void startMovinRoad() // начинаем движение дороги
    {
        if (!isRoadMoving)
        {
            isRoadMoving = true;
            StartCoroutine("RoadMoving");            
        }
    }

    public void stopMovinRoad() //остановка движения дороги
    {
        if (isRoadMoving)
        {
            isRoadMoving = false;
            StopCoroutine("RoadMoving");
        }        
    }   

    IEnumerator RoadMoving() // смещение блоков дороги в течении одного кадра
    {
        float speed;
        while (true)
        {
            speed = speedRoadMove; // увеличиваем скорость движения блоков только когда все три были смещены на равнозначное число
            foreach (GameObject roadBlock in roadFactory.ActiveRoadBlock)
            {
                roadBlock.transform.position = new Vector3(0, 0, roadBlock.transform.position.z - speed);
            }
            yield return null;
        }
    }

    public void resetRoad() // разобрать дорогу
    {
        if (roadFactory.ActiveRoadBlock.Count !=0)
        {
            roadFactory.makeOutRoad();
        }
    }   
}
