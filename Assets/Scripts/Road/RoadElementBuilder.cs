﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadElementBuilder // создаем жлемент из префаба по имени
{
    private string folderName = "Prefabs/RoadElement";
    private Object[] roadElements;
    public RoadElementBuilder()
    {
        roadElements = Resources.LoadAll(folderName, typeof(Object)); // загрузка префабов
    }

    public GameObject createElement(string nameElement) // создание GO из префаба
    {
        Object roadElementPrefab = System.Array.Find(roadElements, prefab => prefab.name == nameElement);
        GameObject roadElement = MonoBehaviour.Instantiate(roadElementPrefab) as GameObject;
        roadElement.name = nameElement;
        return roadElement;
    }    
}