﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadFactory // создание дороги из блоков дороги
{
    GameObject road;
    int countRoadBlock;
    RoadBlockFactory roadBlockFactory;
    LinkedList<GameObject> activeRoadBlock = new LinkedList<GameObject>();
    public LinkedList<GameObject> ActiveRoadBlock { get => activeRoadBlock; set => activeRoadBlock = value; }

    public RoadFactory(GameObject road, int countRoadBlock)
    {
        this.road = road; // то куда положить дорогу
        this.countRoadBlock = countRoadBlock; // кол-во блоков дороги
        roadBlockFactory = new RoadBlockFactory();        
    }

    public void initializeRoad() // Создание первой дороги
    {
        roadBlockFactory.addRoadBlockToRoad = addRoadBlockToRoad; // используем при выходе игрока с блока дороги (переделать)

        for (int i = 0; i < this.countRoadBlock; i++) // создаем блоки дороги и "связываем" их в списке
        {
            GameObject roadBlock = (i > 0) ? roadBlockFactory.getRoadBlock(TypeRoadBlock.Deafult) : roadBlockFactory.getRoadBlock(TypeRoadBlock.Start);
            LinkedListNode<GameObject> roadBlockNode = activeRoadBlock.AddLast(roadBlock);
            roadBlockNode.Value.transform.parent = road.transform;
            if (roadBlockNode.Previous != null) // Соединяем, начиная со второго, блок с предыдущим
            {
                Transform connect_0 = roadBlockNode.Previous.Value.GetComponent<RoadBlock>().ConnectionTop;
                roadBlockNode.Value.transform.position = new Vector3(connect_0.position.x, connect_0.position.y, connect_0.position.z + 6);
            }
        }
    }

    void addRoadBlockToRoad(GameObject roadBlock) // добавляем блок дороги в дорогу (делегат который нужно переделать, этого здесь быть не должно)
    {
        activeRoadBlock.RemoveFirst();

        LinkedListNode<GameObject> roadBlockNode = activeRoadBlock.AddLast(roadBlock);
        roadBlockNode.Value.transform.parent = road.transform;
        Transform connect_0 = roadBlockNode.Previous.Value.GetComponent<RoadBlock>().ConnectionTop;
        roadBlockNode.Value.transform.position = new Vector3(connect_0.position.x, connect_0.position.y, connect_0.position.z + 6);
    }

    public void makeOutRoad() // разбраем дорогу и очищаем список активных блоков дороги
    {
        foreach (GameObject roadBlock in activeRoadBlock)
        {
            roadBlockFactory.makeOutRoadBlock(roadBlock);
        }

        activeRoadBlock.Clear();
    }

}
