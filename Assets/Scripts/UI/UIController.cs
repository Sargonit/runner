﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public enum UIState { // состояния меню, вход состояние происходит когда менб становиться активным
    MainMenu, 
    GameMenu, 
    GameUI
};
public class UIController : MonoBehaviour // управление меню
{
    public delegate void Restart();
    public Restart restart;

    public ReactiveProperty<UIState> CurrentUIState { get; private set; }
    public Text ScoreText { get => scoreText; private set => scoreText = value; }

    [SerializeField] GameObject mainMenu;
    [SerializeField] GameObject gameMenu;
    [SerializeField] GameObject gameUI;
    [SerializeField] Text scoreText;

    GameController GC;

    void Awake()
    {
        CurrentUIState = new ReactiveProperty<UIState>(UIState.MainMenu);
    }

    void Start()
    {
        GameController GC = GameController.Instance;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) // реакция на ESC - скрыть показать игровое меню в только во время игры
        {
            switch (CurrentUIState.Value)
            {
                case UIState.GameMenu:
                    changeState(UIState.GameUI);
                    break;
                case UIState.GameUI:
                    changeState(UIState.GameMenu);
                    break;
                default:
                    break;
            }            
        }
    }    

    public void changeState(UIState state) // реакция на изменение состояния меню
    {
        switch (state)
        {
            case UIState.MainMenu:
                changeToMainManuState();
                break;
            case UIState.GameMenu:
                changeToGameMenuState();
                break;
            case UIState.GameUI:
                changeToGameUIState();
                break;
            default:
                break;
        }
    }

    void changeToMainManuState()
    {
        mainMenu.SetActive(true);
        gameMenu.SetActive(false);
        gameUI.SetActive(false);

        CurrentUIState.Value = UIState.MainMenu;
    }

    void changeToGameMenuState()
    {
        gameMenu.SetActive(true);

        CurrentUIState.Value = UIState.GameMenu;
    }

    void changeToGameUIState()
    {
        mainMenu.SetActive(false);
        gameMenu.SetActive(false);
        gameUI.SetActive(true);

        CurrentUIState.Value = UIState.GameUI;
    }
    public void startGame() // Кнопка Start в главном меню
    {
        changeState(UIState.GameUI);
    }

    public void restartGame() // Кнопка Restart в игровом меню
    {
        restart();
        changeState(UIState.GameUI);
    }

    public void toMainMenu() // Кнопка Main Menu в игровом меню
    {
        changeState(UIState.MainMenu);
    }

    public void exitGame() // Кнопка Exit в главном и игровом меню
    {
        Application.Quit();
    }

}